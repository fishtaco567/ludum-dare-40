﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    Transform target;

    [SerializeField]
    Vector3 offset;

    [SerializeField]
    Camera attatchedCamera;

	// Use this for initialization
	void Start () {
        if (target == null) {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }

        if (attatchedCamera == null) {
            attatchedCamera = GetComponentInChildren<Camera>();
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        var ms = -Input.GetAxis("Mouse ScrollWheel") * 3;
        offset.y += ms;
        if(offset.y < 8) {
            offset.y = 8;
        } else if (offset.y > 20) {
            offset.y = 20;
        } 
        transform.position = target.position + offset;
        transform.LookAt(target);
	}
}
