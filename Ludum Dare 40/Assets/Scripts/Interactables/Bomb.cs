﻿using UnityEngine;

public class Bomb : MonoBehaviour {

    public float range = 12;
    public float damage = 10;
    public float fuseTime = 3;
    float aliveTime;

    [SerializeField]
    GameObject effects;

    [SerializeField]
    GameObject[] gold;

    private void Start() {

    }

    private void Update() {
        aliveTime += Time.deltaTime;
        if(fuseTime < aliveTime) {
            //Play vfx/sfx
            var things = Physics.OverlapSphere(transform.position, range);
            foreach(var thing in things) {
                var enemy = thing.GetComponent<Enemy>();
                var player = thing.GetComponent<PlayerController>();
                if(enemy != null) {
                    enemy.Health -= damage;
                }
                if(player != null) {
                    player.Health -= damage;
                }
                if(thing.CompareTag("Barrel")) {
                    Destroy(thing.gameObject);
                    Instantiate(gold[Random.Range(0, 4)], transform.position, transform.rotation);
                }
            }

            Destroy(Instantiate(effects, transform.position, transform.rotation), 4);

            Destroy(gameObject);
        }
    }

}
