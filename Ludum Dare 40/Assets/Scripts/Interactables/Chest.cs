﻿using UnityEngine;

namespace Interactables {
    public class Chest : Interactable {

        [SerializeField]
        int bombs;
        [SerializeField]
        int health;
        [SerializeField]
        int coin;
        [SerializeField]
        bool swordUpgrade;

        Animator anim;

        AudioSource source;

        bool opened;

        [SerializeField]
        GameObject bombP;
        [SerializeField]
        GameObject coinP;
        [SerializeField]
        GameObject heartP;

        protected void Start() {
            anim = GetComponent<Animator>();
            if(Random.Range(0, 50) == 0) {
                swordUpgrade = true;
            }
            if(Random.Range(0, 7) == 3) {
                health = Random.Range(1, 5);
                if(Random.Range(0, 20) == 0) {
                    health = 10;
                }
                bombs = 0;
            } else if (Random.Range(0, 5) == 3) {
                coin = Random.Range(1, 35);
                bombs = 0;
            } else {
                bombs = 1;
            }
            source = GetComponent<AudioSource>();
        }

        public override void Interact(PlayerController player) {
            if(!opened) {
                anim.SetBool("Opened", true);
                source.Play();
                player.Bombs += bombs;
                player.Health += health;
                player.GetComponent<ResourceCollector>().Gold += coin;

                if(bombs != 0) {
                    bombP.SetActive(true);
                    bombP.GetComponent<ParticleSystem>().Play();
                }

                if (coin != 0) {
                    coinP.SetActive(true);
                    coinP.GetComponent<ParticleSystem>().Play();
                }

                if (health != 0) {
                    heartP.SetActive(true);
                    heartP.GetComponent<ParticleSystem>().Play();
                }

                if(swordUpgrade) {
                    player.damage = 7;
                    player.sword1.SetActive(false);
                    player.sword2.SetActive(true);
                }
                opened = true;
            }
        }

    }
}
