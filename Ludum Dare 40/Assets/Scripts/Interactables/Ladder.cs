﻿using UnityEngine;

public class Ladder : Interactable {

    public int location;
    int target;

    public void Start() {
        target = Random.Range(0, 2);
    }

    public override void Interact(PlayerController player) {
        if (target == location) {
            target = 2;
        }

        var pos = new Vector3(0, 0, 0);

        for (int i = 0; i < 10; i++) {
            switch (target) {
                case 0:
                    pos = Random.onUnitSphere * 75;
                    pos.y = 5;
                    pos += new Vector3(GameManager.world1pos + 100, 0, GameManager.world1pos + 100);
                    break;
                case 1:
                    pos = Random.onUnitSphere * 75;
                    pos.y = 5;
                    pos += new Vector3(GameManager.world2pos + 100, 0, GameManager.world2pos + 100);
                    break;
                case 2:
                    pos = Random.onUnitSphere * 75;
                    pos.y = 5;
                    pos += new Vector3(GameManager.world3pos + 100, 0, GameManager.world3pos + 100);
                    player.transform.position = pos;
                    break;
            }
            
            RaycastHit hit;
            if (Physics.Raycast(pos + Vector3.up * 5, Vector3.down, out hit, 10)) {
                if (hit.point.y < 0.5f) {
                    pos.y = 0;
                    break;
                }
            }
        }

        player.transform.position = pos;
    }

}

