﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField]
    Material caveWalls;
    [SerializeField]
    Material caveFloors;

    [SerializeField]
    Material dungeonWalls;
    [SerializeField]
    Material dungeonFloors;

    [SerializeField]
    Material castleWalls;
    [SerializeField]
    Material castleFloors;
    
    public static GameObject player;
    [SerializeField]
    Text health;
    [SerializeField]
    Image bar;
    [SerializeField]
    Text coins;
    [SerializeField]
    Text bombs;

    MapBuilder mapB;

    [SerializeField]
    GameObject[] enemies;
    [SerializeField]
    GameObject[] enemiesHard;
    [SerializeField]
    GameObject[] enemiesHardest;
    [SerializeField]
    float baseSpawnRate;
    float timeSinceLast;

    float spawnRate;

    int width = 200;
    int height = 200;

    [SerializeField]
    float spawnCoefficient;

    public static int world1pos = 0;
    public static int world2pos = 300;
    public static int world3pos = 600;

    public static float volume = 1;

    [SerializeField]
    GameObject gameoverText;

	// Use this for initialization
	void Awake () {
        MapBuilder.TextureSet dungSet = new MapBuilder.TextureSet(dungeonWalls, dungeonFloors, 4, 1);
        MapBuilder.TextureSet caveSet = new MapBuilder.TextureSet(caveWalls, caveFloors, 3, 1);
        MapBuilder.TextureSet castleSet = new MapBuilder.TextureSet(castleWalls, castleFloors, 4, 3);

        mapB = GetComponent<MapBuilder>();

        mapB.GenerateMap(0, Random.Range(0, 1000000), caveSet, world1pos, world1pos, width, height, 45, 60, 4, 8, 12);

        mapB.GenerateMap(1, Random.Range(0, 1000000), dungSet, world2pos, world2pos, width, height, 30, 40, 15, 20, 6);

        mapB.GenerateMap(2, Random.Range(0, 1000000), castleSet, world3pos, world3pos, width, height, 25, 30, 25, 35, 2);

        player = GameObject.FindGameObjectWithTag("Player");
	}

    public void Quit() {
        SceneManager.LoadScene(0);
    }
	
	// Update is called once per frame
	void Update () {
        AudioListener.volume = volume;

        var pc = player.GetComponent<PlayerController>();

        health.text = pc.Health + "/" + pc.MaxHealth;
        bar.fillAmount = pc.Health / pc.MaxHealth;

        var rc = player.GetComponent<ResourceCollector>();

        coins.text = Mathf.RoundToInt(rc.Gold).ToString();
        bombs.text = Mathf.RoundToInt(pc.Bombs).ToString();

        timeSinceLast += Time.deltaTime;

        if (player.transform.position.x > 600) {
            spawnRate = baseSpawnRate - 1.3f - Mathf.Pow((float)rc.Gold, spawnCoefficient);
        } else if (player.transform.position.x > 300) {
            spawnRate = baseSpawnRate - .6f - Mathf.Pow((float)rc.Gold, spawnCoefficient);
        } else {
            spawnRate = baseSpawnRate - Mathf.Pow((float)rc.Gold, spawnCoefficient);
        }

        if(timeSinceLast > (Random.value - 0.5) * spawnRate * .3f + spawnRate) {
            SpawnEnemy();
        }

        if(pc.Health < 0 && !gameoverText.activeSelf) {
            gameoverText.SetActive(true);
            var gameover = gameoverText.GetComponent<Text>();
            gameover.text = gameover.text.Replace("xxxx", rc.Gold.ToString());
        }

        if(pc.DeadTime > 5) {
            if(Input.GetButtonDown("Fire1")) {
                SceneManager.LoadScene(0);
            }
        }
	}

    void SpawnEnemy() {
        var enemy = enemies[Random.Range(0, enemies.Length)];
        if (player.transform.position.x > 600 && Random.Range(0, 10) == 0) {
            enemy = enemiesHardest[Random.Range(0, enemiesHardest.Length)];
        } else if(player.transform.position.x > 300 && Random.Range(0, 10) == 0) {
            enemy = enemiesHard[Random.Range(0, enemiesHard.Length)];
        }
        for(int i = 0; i < 10; i++) {
            var o1 = Random.insideUnitSphere;
            o1.y = 0;
            o1 = o1 * (Random.value + .5f) * 25;
            var pos = player.transform.position + o1;
            RaycastHit hit;
            if(Physics.Raycast(pos + Vector3.up * 5, Vector3.down, out hit, 10)) {
                if(hit.point.y < 0.5f) {
                    Instantiate(enemy, pos, Quaternion.identity);
                    timeSinceLast = 0;
                    break;
                }
            }
        }

    }
}
