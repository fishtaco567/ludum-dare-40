﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    [SerializeField]
    GameObject credits;

    [SerializeField]
    Slider vol;

    [SerializeField]
    GameObject help;

	void Start () {
		
	}
	
	void Update () {
        GameManager.volume = vol.value;
        AudioListener.volume = vol.value;
        if(Input.GetButtonDown("Jump")) {
            Play();
        }
	}

    public void Help() {
        help.SetActive(!help.activeSelf);
    }

    public void Credits() {
        credits.SetActive(!credits.activeSelf);
    }

    public void Play() {
        SceneManager.LoadScene(1);
    }

    public void Exit() {
        Application.Quit();
    }

}
