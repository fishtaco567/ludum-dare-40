﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using System;

public class MapBuilder : MonoBehaviour {

    public GameObject[] props;

    public GameObject[] gold;

    public GameObject[] wallProps;

    public GameObject[] ladders;

    public enum Tile {
        Open,
        Wall
    }

    const float TILE_SIZE = 1;
    const int WALL_HEIGHT = 3;

    public struct Pair {

        public Vector2 start;
        public Vector2 end;

        public Pair(Vector2 start, Vector2 end) {
            this.start = start;
            this.end = end;
        }

    }

    public struct TextureSet {
        public Material walls;
        public Material floors;
        public int wallVariations;
        public int floorVariations;
        
        public TextureSet(Material walls, Material floors, int wallVariations, int floorVariations) {
            this.walls = walls;
            this.floors = floors;
            this.wallVariations = wallVariations;
            this.floorVariations = floorVariations;
        }
    }

    public Tile[] map;

    public void GenerateMap(int location, int seed, TextureSet textures, float x, float y, int width, int height, int minRooms, int maxRooms, int roomMinSize, int roomMaxSize, int maxExtraHalls) {
        Tile[] tiles = new Tile[width * height];

        for(int i = 0; i < tiles.Length; i++) {
            tiles[i] = Tile.Wall;
        }

        System.Random rand = new System.Random(seed);

        List<Vector2> roomNodes = new List<Vector2>();

        int numRooms = rand.Next(minRooms, maxRooms);
        for(int i = 0; i < numRooms; i++) {
            int roomWidth = rand.Next(roomMinSize, roomMaxSize);
            int roomHeight = rand.Next(roomMinSize, roomMinSize);
            int roomX = rand.Next(5, width - roomWidth - 5);
            int roomY = rand.Next(5, height - roomHeight - 5);

            carveRoom(roomX, roomY, roomWidth, roomHeight, ref tiles, width);
            roomNodes.Add(new Vector2(roomX, roomY));
        }

        List<Pair> corridors = new List<Pair>();

        List<Vector2> open = new List<Vector2>(roomNodes.ToArray());
        while(open.Count > 1) {
            Vector2 start = open[0];
            open.Remove(start);
            float minDist = int.MaxValue;
            Vector2 end = new Vector2(0, 0);
            foreach(Vector2 room in open) {
                float dist = Vector2.Distance(start, end);
                if (dist < minDist) {
                    minDist = dist;
                    end = room;
                }
            }
            if (location > 1) {
                AddFancyCorridor(corridors, start, end);
            } else if (location > 0 && UnityEngine.Random.Range(0, 8) > 1) {
                AddFancyCorridor(corridors, start, end);
            }
            else {
                corridors.Add(new Pair(start, end));
            }
        }

        int numHalls = rand.Next(0, maxExtraHalls);
        for(int i = 0; i < numHalls; i++) {
            if (location > 0) {
                AddFancyCorridor(corridors, roomNodes[rand.Next(roomNodes.Count)], roomNodes[rand.Next(roomNodes.Count)]);
            } else if (location > 0 && UnityEngine.Random.Range(0, 8) > 1) {
                AddFancyCorridor(corridors, roomNodes[rand.Next(roomNodes.Count)], roomNodes[rand.Next(roomNodes.Count)]);
            } else {
                corridors.Add(new Pair(roomNodes[rand.Next(roomNodes.Count)], roomNodes[rand.Next(roomNodes.Count)]));
            }
        }

        foreach(var pair in corridors) {
            carveCorridor(pair.start, pair.end, ref tiles, width);
            if ((Mathf.Abs(Vector2.Angle(Vector2.up, (pair.end - pair.start))) > 45 && (Mathf.Abs(Vector2.Angle(Vector2.up, (pair.end - pair.start))) < 135))) {
                Vector2 offset = new Vector2(0, 1);
                carveCorridor(pair.start + offset, pair.end + offset, ref tiles, width);
                carveCorridor(pair.start + offset, pair.end + offset * 2, ref tiles, width);
                carveCorridor(pair.start - offset, pair.end - offset, ref tiles, width);
                carveCorridor(pair.start - offset, pair.end - offset * 2, ref tiles, width);
            } else {
                Vector2 offset = new Vector2(1, 0);
                carveCorridor(pair.start + offset, pair.end + offset, ref tiles, width);
                carveCorridor(pair.start + offset, pair.end + offset * 2, ref tiles, width);
                carveCorridor(pair.start - offset, pair.end - offset, ref tiles, width);
                carveCorridor(pair.start - offset, pair.end - offset * 2, ref tiles, width);
            }
        }

        BuildMesh(location, textures, x, y, tiles, width, height, rand);
        map = tiles;
    }

    private void AddFancyCorridor(List<Pair> corridors, Vector2 node, Vector2 node2) {
        var middle = new Vector2(node.x, node2.y);
        corridors.Add(new Pair(node, middle));
        corridors.Add(new Pair(middle, node2));
    }

    private void BuildMesh(int location, TextureSet textures, float x, float y, Tile[] tiles, int width, int height, System.Random rand) {
        List<Vector3> upperVerts = new List<Vector3>();
        List<Vector2> upperTex = new List<Vector2>();
        List<int> upperIndicies = new List<int>();

        List<Vector3> lowerVerts = new List<Vector3>();
        List<Vector2> lowerTex = new List<Vector2>();
        List<int> lowerIndicies = new List<int>();

        List<Vector3> wallsVerts = new List<Vector3>();
        List<Vector2> wallsTex = new List<Vector2>();
        List<int> wallsIndicies = new List<int>();

        int lIndex = 0;
        int uIndex = 0;
        int wIndex = 0;

        int splitSize = 65532;
        int splitSizeVerts = (splitSize / 6) * 4;

        for (int i = 0; i < width; i++) {
            for(int j = 0; j < width; j++) {
                Tile curTile = tiles[i + j * width];
                #region
                switch (curTile) {
                    case Tile.Open:
                        if(rand.Next(0, 180 - location * 40) == 0) {
                            Instantiate(props[rand.Next(0, props.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, 0, y + (j + .5f) * TILE_SIZE), Quaternion.identity);
                        }

                        if (rand.Next(0, 25 - location * 6) == 0) {
                            var goldNum = 0;
                            goldNum += UnityEngine.Random.Range(0, 20);
                            if(goldNum < 8 - location * 2f) {
                                goldNum = 0;
                            } else if(goldNum < 13 - location * 2f) {
                                goldNum = 1;
                            } else if(goldNum < 17 - location * 1.5f) {
                                goldNum = 2;
                            } else {
                                goldNum = 3;
                            }
                            Instantiate(gold[rand.Next(0, gold.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, 0, y + (j + .5f) * TILE_SIZE), Quaternion.identity);
                        }

                        lowerVerts.Add(new Vector3(x + i * TILE_SIZE, 0, y + j * TILE_SIZE));
                        lowerVerts.Add(new Vector3(x + i * TILE_SIZE, 0, y + (j + 1) * TILE_SIZE));
                        lowerVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, 0, y + (j + 1) * TILE_SIZE));
                        lowerVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, 0, y + j * TILE_SIZE));

                        AddTextures(lowerTex, textures.floorVariations, rand);

                        lowerIndicies.Add(lIndex + 0);
                        lowerIndicies.Add(lIndex + 1);
                        lowerIndicies.Add(lIndex + 2);
                        lowerIndicies.Add(lIndex + 0);
                        lowerIndicies.Add(lIndex + 2);
                        lowerIndicies.Add(lIndex + 3);

                        lIndex += 4;
                        break;
                    case Tile.Wall:
                        upperVerts.Add(new Vector3(x + i * TILE_SIZE, WALL_HEIGHT * TILE_SIZE, y + j * TILE_SIZE));
                        upperVerts.Add(new Vector3(x + i * TILE_SIZE, WALL_HEIGHT * TILE_SIZE, y + (j + 1) * TILE_SIZE));
                        upperVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, WALL_HEIGHT * TILE_SIZE, y + (j + 1) * TILE_SIZE));
                        upperVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, WALL_HEIGHT * TILE_SIZE, y + j * TILE_SIZE));

                        AddTextures(upperTex, 1, rand);

                        upperIndicies.Add(uIndex + 0);
                        upperIndicies.Add(uIndex + 1);
                        upperIndicies.Add(uIndex + 2);
                        upperIndicies.Add(uIndex + 0);
                        upperIndicies.Add(uIndex + 2);
                        upperIndicies.Add(uIndex + 3);

                        uIndex += 4;
                        break;
                }
                #endregion

                if(i < width - 1) {
                    Tile rightTile = tiles[i + 1 + j * width];
                    if(curTile != rightTile) {
                        float yIndex = 0;

                        for(int k = 0; k < WALL_HEIGHT; k++) {
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex, y + j * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex + TILE_SIZE, y + j * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex + TILE_SIZE, y + (j + 1) * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex, y + (j + 1) * TILE_SIZE));

                            AddTextures(wallsTex, textures.wallVariations, rand);

                            if (curTile == Tile.Wall) {
                                if (rand.Next(0, 80) == 0) {
                                    Instantiate(wallProps[rand.Next(0, wallProps.Length)], new Vector3(x + (i + 1) * TILE_SIZE, TILE_SIZE * 1.5f, y + (j + .5f) * TILE_SIZE), Quaternion.AngleAxis(90, Vector3.up));
                                }
                                if(rand.Next(0, 300) == 0) {
                                    var ladder = Instantiate(ladders[rand.Next(0, ladders.Length)], new Vector3(x + (i + 1) * TILE_SIZE, 0, y + (j + .5f) * TILE_SIZE), Quaternion.AngleAxis(90, Vector3.up));
                                    ladder.GetComponent<Ladder>().location = location;
                                }
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 1);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 3);
                            } else {
                                if (rand.Next(0, 80) == 0) {
                                    Instantiate(wallProps[rand.Next(0, wallProps.Length)], new Vector3(x + (i + 1) * TILE_SIZE, TILE_SIZE * 1.5f, y + (j + .5f) * TILE_SIZE), Quaternion.AngleAxis(-90, Vector3.up));
                                }
                                if (rand.Next(0, 300) == 0) {
                                    var ladder = Instantiate(ladders[rand.Next(0, ladders.Length)], new Vector3(x + (i + 1) * TILE_SIZE, 0, y + (j + .5f) * TILE_SIZE), Quaternion.AngleAxis(-90, Vector3.up));
                                    ladder.GetComponent<Ladder>().location = location;
                                }
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 1);
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 3);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 0);
                            }

                            wIndex += 4;
                            yIndex += TILE_SIZE;
                        } 
                    }
                }

                if(j < height - 1) {
                    Tile upTile = tiles[i + (j + 1) * width];
                    if(curTile != upTile) {
                        float yIndex = 0;

                        for (int k = 0; k < WALL_HEIGHT; k++) {
                            wallsVerts.Add(new Vector3(x + i * TILE_SIZE, yIndex, y + (j + 1) * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + i * TILE_SIZE, yIndex + TILE_SIZE, y + (j + 1) * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex + TILE_SIZE, y + (j + 1) * TILE_SIZE));
                            wallsVerts.Add(new Vector3(x + (i + 1) * TILE_SIZE, yIndex, y + (j + 1) * TILE_SIZE));

                            AddTextures(wallsTex, textures.wallVariations, rand);

                            if (curTile == Tile.Open) {
                                if (rand.Next(0, 80) == 0) {
                                    Instantiate(wallProps[rand.Next(0, wallProps.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, TILE_SIZE * 1.5f, y + (j + 1) * TILE_SIZE), Quaternion.AngleAxis(180, Vector3.up));
                                }
                                if (rand.Next(0, 300) == 0) {
                                    var ladder = Instantiate(ladders[rand.Next(0, ladders.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, 0, y + (j + 1) * TILE_SIZE), Quaternion.AngleAxis(180, Vector3.up));
                                    ladder.GetComponent<Ladder>().location = location;
                                }
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 1);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 3);
                            } else {
                                if (rand.Next(0, 80) == 0) {
                                    Instantiate(wallProps[rand.Next(0, wallProps.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, TILE_SIZE * 1.5f, y + (j + 1) * TILE_SIZE), Quaternion.identity);
                                }
                                if (rand.Next(0, 300) == 0) {
                                    var ladder = Instantiate(ladders[rand.Next(0, ladders.Length)], new Vector3(x + (i + .5f) * TILE_SIZE, 0, y + (j + 1) * TILE_SIZE), Quaternion.identity);
                                    ladder.GetComponent<Ladder>().location = location;
                                }
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 1);
                                wallsIndicies.Add(wIndex + 0);
                                wallsIndicies.Add(wIndex + 3);
                                wallsIndicies.Add(wIndex + 2);
                                wallsIndicies.Add(wIndex + 0);
                            }

                            wIndex += 4;
                            yIndex += TILE_SIZE;
                        }
                    }
                }
            }
        }

        var upperVerts2 = upperVerts.ChunkBy<Vector3>(splitSizeVerts);
        var upperUvs2 = upperTex.ChunkBy<Vector2>(splitSizeVerts);
        var upperIndicies2 = upperIndicies.ChunkBy<int>(splitSize);

        for (int i = 0; i < upperVerts2.Count; i++) {
            int start = upperIndicies2[i][0];
            var ui2 = upperIndicies2[i].Select((index) => index - start);
            Mesh upper = new Mesh();
            upper.vertices = upperVerts2[i].ToArray();
            upper.uv = upperUvs2[i].ToArray();
            upper.triangles = ui2.ToArray();
            upper.RecalculateNormals();

            GameObject upperObject = new GameObject();
            upperObject.transform.position = new Vector3(0, 0, 0);
            MeshFilter upperFilter = upperObject.AddComponent<MeshFilter>();
            MeshRenderer upperRenderer = upperObject.AddComponent<MeshRenderer>();
            var upperPhys = upperObject.AddComponent<MeshCollider>();
            upperFilter.mesh = upper;
            upperPhys.sharedMesh = upper;
            upperRenderer.material = textures.walls;
        }
        
        var lowerVerts2 = lowerVerts.ChunkBy<Vector3>(splitSizeVerts);
        var lowerUvs2 = lowerTex.ChunkBy<Vector2>(splitSizeVerts);
        var lowerIndicies2 = lowerIndicies.ChunkBy<int>(splitSize);

        for (int i = 0; i < lowerVerts2.Count; i++) {
            int start = lowerIndicies2[i][0];
            var li2 = lowerIndicies2[i].Select((index) => index - start);
            Mesh lower = new Mesh();
            lower.vertices = lowerVerts2[i].ToArray();
            lower.uv = lowerUvs2[i].ToArray();
            lower.triangles = li2.ToArray();
            lower.RecalculateNormals();

            GameObject lowerObject = new GameObject();
            lowerObject.transform.position = new Vector3(0, 0, 0);
            MeshFilter lowerFilter = lowerObject.AddComponent<MeshFilter>();
            MeshRenderer lowerRenderer = lowerObject.AddComponent<MeshRenderer>();
            var lowerPhys = lowerObject.AddComponent<MeshCollider>();
            lowerFilter.mesh = lower;
            lowerPhys.sharedMesh = lower;
            lowerRenderer.material = textures.floors;
        }

        var wallsVerts2 = wallsVerts.ChunkBy<Vector3>(splitSizeVerts);
        var wallsUvs2 = wallsTex.ChunkBy<Vector2>(splitSizeVerts);
        var wallsIndicies2 = wallsIndicies.ChunkBy<int>(splitSize);

        for(int i = 0; i < wallsVerts2.Count; i++) {
            int start = wallsIndicies2[i][0] - wallsIndicies2[i][0] % 4;
            var wi2 = wallsIndicies2[i].Select((index) => index - start);
            Mesh walls = new Mesh();
            walls.vertices = wallsVerts2[i].ToArray();
            walls.uv = wallsUvs2[i].ToArray();
            walls.triangles = wi2.ToArray();
            walls.RecalculateNormals();

            GameObject wallObject = new GameObject();
            wallObject.transform.position = new Vector3(0, 0, 0);
            MeshFilter wallFilter = wallObject.AddComponent<MeshFilter>();
            MeshRenderer wallRenderer = wallObject.AddComponent<MeshRenderer>();
            var wallPhys = wallObject.AddComponent<MeshCollider>();
            wallFilter.mesh = walls;
            wallPhys.sharedMesh = walls;
            wallRenderer.material = textures.walls;
        }
    }

    private static void AddTextures(List<Vector2> textures, int variations, System.Random rand) {
        Vector2 ulLl = new Vector2(0f, .5f);
        Vector2 ulUl = new Vector2(0f, 1f);
        Vector2 ulUr = new Vector2(.5f, 1f);
        Vector2 ulLr = new Vector2(.5f, .5f);

        Vector2 urLl = new Vector2(.5f, .5f);
        Vector2 urUl = new Vector2(.5f, 1);
        Vector2 urUr = new Vector2(1, 1);
        Vector2 urLr = new Vector2(1, .5f);

        Vector2 dlLl = new Vector2(0, 0);
        Vector2 dlUl = new Vector2(0, .5f);
        Vector2 dlUr = new Vector2(.5f, .5f);
        Vector2 dlLr = new Vector2(.5f, 0);

        Vector2 drLl = new Vector2(.5f, 0);
        Vector2 drUl = new Vector2(.5f, 1);
        Vector2 drUr = new Vector2(1, .5f);
        Vector2 drLr = new Vector2(1, 0);

        int randomNumber = rand.Next(0, 100);

        if (randomNumber < 90 || variations < 2) {
            textures.Add(ulLl);
            textures.Add(ulUl);
            textures.Add(ulUr);
            textures.Add(ulLr);
        } else if(randomNumber < 94 || variations < 3) {
            textures.Add(urLl);
            textures.Add(urUl);
            textures.Add(urUr);
            textures.Add(urLr);
        } else if (randomNumber < 97 || variations < 4) {
            textures.Add(dlLl);
            textures.Add(dlUl);
            textures.Add(dlUr);
            textures.Add(dlLr);
        } else {
            textures.Add(drLl);
            textures.Add(drUl);
            textures.Add(drUr);
            textures.Add(drLr);
        }
    }

    private static void carveRoom(int x, int y, int width, int height, ref Tile[] tiles, int mapWidth) {
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                tiles[(i + x) + mapWidth * (j + y)] = Tile.Open;
            }
        }
    }

    private static void carveCorridor(Vector2 start, Vector2 end, ref Tile[] tiles, int mapWidth) {
        int x = (int)start.x;
        int y = (int)start.y;
        int x2 = (int)end.x;
        int y2 = (int)end.y;
        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);
        if (!(longest > shortest)) {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++) {
            int index = x + y * mapWidth;
            if (index < tiles.Length && index > 0) {
                tiles[index] = Tile.Open;
            }
            numerator += shortest;
            if (!(numerator < longest)) {
                numerator -= longest;
                x += dx1;
                y += dy1;
            } else {
                x += dx2;
                y += dy2;
            }
        }
    }

}
