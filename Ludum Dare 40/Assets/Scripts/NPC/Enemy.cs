﻿using System;
using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(Animator))]
public class Enemy : MonoBehaviour {

    [SerializeField]
    private float _maxHealth;
    public float MaxHealth {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }

    public bool IsDying { get; set; }

    [SerializeField]
    private float _health;
    public float Health {
        get { return _health; }
        set {
            if(value < _health) {
                source.Play();
            }
            _health = value;
        }
    }

    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float damage;

    [SerializeField]
    private float maxIdle;

    [SerializeField]
    private float maxMove;

    [SerializeField]
    private float aggroRadius;

    [SerializeField]
    private float attackCooldown;

    [SerializeField]
    private float attackWindup;

    private Animator animator;

    private CharacterController controller;

    float moveTime;
    float idleTime;
    float curSpeed;
    float cooldown;
    float windup;
    bool attacking;

    float speedMod = 0;

    float deathTime;
    Quaternion targetRotationDead;

    AudioSource source;

    public virtual void Start() {
        _health = MaxHealth;
        source = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        moveTime = 0;
        idleTime = (float) new System.Random().NextDouble() * maxIdle;
    }

    public virtual void Update() {
        if(Vector3.Distance(transform.position, GameManager.player.transform.position) > 100) {
            Destroy(gameObject);
        }

        if(GameManager.player.transform.position.x > 600) {
            speedMod = GameManager.player.GetComponent<ResourceCollector>().Gold * .001f + 1.5f;
        } else if(GameManager.player.transform.position.x > 300) {
            speedMod = GameManager.player.GetComponent<ResourceCollector>().Gold * .0005f + .75f;
        } else {
            speedMod = GameManager.player.GetComponent<ResourceCollector>().Gold * .0005f;
        }

        if(_health <= 0 && !IsDying) {
            IsDying = true;
            targetRotationDead = Quaternion.AngleAxis(85, transform.right);
            deathTime = 0;
        }

        if(IsDying) {
            deathTime += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotationDead, Time.deltaTime * 3);
            if(deathTime > 2) {
                Destroy(gameObject);
            } 
            return;
        }

        if (!attacking) {
            if (idleTime > 0) {
                curSpeed = 0;
                idleTime -= Time.deltaTime;
                if(idleTime < 0) {
                    moveTime = (float)new System.Random().NextDouble() * maxMove;
                }
            }

            if (moveTime > 0) {
                curSpeed = moveSpeed + speedMod;

                var t = UnityEngine.Random.insideUnitCircle * 20;
                var target = new Vector3(t.x, 0, t.y);
                var players = Physics.OverlapSphere(transform.position, aggroRadius, 1 << 10);
                if(players.Length != 0) {
                    target = players[0].transform.position;
                }
                var mov = (target - transform.position).normalized * curSpeed * Time.deltaTime;
                mov.y = -3;
                controller.Move(mov);
                transform.rotation = Quaternion.LookRotation(target - transform.position);

                moveTime -= Time.deltaTime;
                if (moveTime < 0) {
                    idleTime = (float)new System.Random().NextDouble() * maxIdle;
                }

            }

            cooldown -= Time.deltaTime;
            var toAttack = Physics.OverlapSphere(transform.position, aggroRadius, 1 << 10);
            if (toAttack.Length != 0 && cooldown < 0) {
                if (Vector3.Distance(toAttack[0].transform.position, transform.position) < 3) {
                    attacking = true;
                    windup = attackWindup;
                }
            }
        }

        if(attacking) {
            curSpeed = 0;
            windup -= Time.deltaTime;
            if(windup < 0) {
                attacking = false;
                idleTime = UnityEngine.Random.value * 1;
                moveTime = 0;
                var toAttack = Physics.OverlapSphere(transform.position, aggroRadius, 1 << 10);
                if (toAttack.Length != 0 && cooldown < 0) {
                    if (Vector3.Distance(toAttack[0].transform.position, transform.position) < 3) {
                        animator.SetTrigger("Attack");
                        cooldown = attackCooldown;
                        var pc = toAttack[0].GetComponent<PlayerController>();
                        if (pc != null) {
                            pc.Health -= damage;
                        }
                    }
                }
            }
        }

        animator.SetFloat("Speed", curSpeed);
    }

}