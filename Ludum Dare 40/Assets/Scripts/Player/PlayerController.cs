﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(Animator))]
public class PlayerController : MonoBehaviour {

    [SerializeField]
    float moveSpeed = 6.5f;

    CharacterController cc;
    GameObject mainCam;

    [SerializeField]
    LayerMask enemyMask;

    [SerializeField]
    float attackDistanceForward;

    [SerializeField]
    Vector3 attackBox;

    public float damage = 5;

    public float MaxHealth = 50;
    private float _health;
    public float Health {
        get {
            return _health;
        }
        set {
            if(value < _health && _health > 0) {
                sources[0].Play();
            }
            _health = value;
            if(_health > MaxHealth) {
                _health = MaxHealth;
            }
        }
    }

    Animator anim;

    [SerializeField]
    private float _bombs;
    public float Bombs {
        get { return _bombs; }
        set { _bombs = value; }
    }

    [SerializeField]
    GameObject bombObj;

    AudioSource[] sources;

    public bool IsDying;
    public float DeadTime = 0;
    Quaternion targetRotationDead;

    public GameObject sword1;
    public GameObject sword2;

    void Start () {
        sources = GetComponents<AudioSource>();
        Bombs = 0;
        Health = MaxHealth;
        cc = GetComponent<CharacterController>();
        mainCam = GameObject.FindGameObjectWithTag("MainCamera");
        anim = GetComponent<Animator>();

        for (int i = 0; i < 10; i++) {
            var pos = Random.onUnitSphere * 75;
            pos.y = 2;
            pos += new Vector3(GameManager.world1pos + 100, 0, GameManager.world1pos + 100);
            RaycastHit hit;
            if (Physics.Raycast(pos + Vector3.up * 5, Vector3.down, out hit, 100)) {
                if (hit.point.y < 2f) {
                    transform.position = pos;
                    break;
                }
            }
        }
    }
	
	void Update () {
        if (_health <= 0 && !IsDying) {
            IsDying = true;
            targetRotationDead = Quaternion.AngleAxis(85, transform.right);
        }

        if (IsDying) {
            DeadTime += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotationDead, Time.deltaTime * 3);
            return;
        }

        DoMovement();
        DoAttack();
	}

    [SerializeField]
    GameObject bombSpot;
    bool holdingBomb = false;

    GameObject curBomb;

    private void DoAttack() {
        bool atk = Input.GetButtonDown("Fire1");

        if(atk) {
            anim.SetTrigger("Attack");

            sources[1].Play();

            var enemies = Physics.OverlapBox(transform.position + transform.forward * 1, attackBox);
            if (enemies.Length != 0) {
                foreach(var collider in enemies) {
                    var enemy = collider.GetComponent<Enemy>();
                    if(enemy != null) {
                        enemy.Health -= damage;
                        //Do vfx/sfx
                    }
                }
            }
        }

        bool bomb = Input.GetButtonDown("Fire2");

        if(!holdingBomb && Input.GetButton("Fire2") && Bombs > 0) {
            holdingBomb = true;
            curBomb = Instantiate(bombObj, bombSpot.transform);
            curBomb.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            Bombs--;
        }

        if(holdingBomb && !Input.GetButton("Fire2")) {
            holdingBomb = false;
            curBomb.transform.parent = null;
            curBomb.transform.position = transform.position + transform.forward;
            curBomb.transform.rotation = Quaternion.identity;
            curBomb = null;
        }

        bool interact = Input.GetButtonDown("Jump");

        if(interact) {
            var interactables = Physics.OverlapBox(transform.position + transform.forward * 1, attackBox);
            if (interactables.Length != 0) {
                foreach (var collider in interactables) {
                    var interactable = collider.GetComponent<Interactable>();
                    if (interactable != null) {
                        interactable.Interact(this);
                        //Do vfx/sfx
                    }
                }
            }
        }
    }

    private void DoMovement() {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        var forward = mainCam.transform.forward;
        forward = Vector3.ProjectOnPlane(forward, Vector3.up).normalized;

        var right = mainCam.transform.right;
        right = Vector3.ProjectOnPlane(right, Vector3.up).normalized;

        var movement = v * forward + h * right;
        if(movement.magnitude > 1) {
            movement = movement.normalized;
        }

        if (!Mathf.Approximately(movement.sqrMagnitude, 0)) {
            transform.rotation = Quaternion.LookRotation(movement, Vector3.up);
        }

        anim.SetFloat("Runspeed", movement.magnitude * moveSpeed);
        if(movement.magnitude > 0.1 && !sources[2].isPlaying) {
            sources[2].Play();
        } else if(movement.magnitude < 0.1) {
            sources[2].Stop();
        }

        movement.y = -2f;

        cc.Move(movement * moveSpeed * Time.deltaTime);
    }

}
