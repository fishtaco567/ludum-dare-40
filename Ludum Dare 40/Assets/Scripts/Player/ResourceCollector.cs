﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interactables;

public class ResourceCollector : MonoBehaviour {

    [SerializeField]
    private float _gold;
    public float Gold {
        get { return _gold; }
        set { _gold = value; }
    }

    [SerializeField]
    float pickupRadius = 5;

    [SerializeField]
    LayerMask goldLayer;

    private void Update() {
        var overlaps = Physics.OverlapSphere(transform.position, pickupRadius, goldLayer);

        foreach(var overlapping in overlaps) {
            var gold = overlapping.GetComponent<GoldPickup>();
            if(gold != null) {
                Gold += gold.value;

                //Run the effects
                var effectsPrefab = gold.pickupEffects;
                if (effectsPrefab != null) {
                    var effects = Instantiate(effectsPrefab, gold.transform.position, gold.transform.rotation);
                    var audioSource = effects.GetComponent<AudioSource>();
                    var particles = effects.GetComponents<ParticleSystem>();

                    if (audioSource != null) {
                        audioSource.Play();
                    }

                    if (particles.Length != 0) {
                        foreach (var particleSystem in particles) {
                            particleSystem.Play();
                        }
                    }

                    Destroy(effects, 2);
                }

                Destroy(gold.gameObject);
            }
        }
    }

}
